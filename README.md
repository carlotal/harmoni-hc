# HARMONI-HC

ELT-HARMONI package for high-contrast simulations

## Installation

from sources

```
> git clone git@gricad-gitlab.univ-grenoble-alpes.fr:carlotal/harmoni-hc.git
> cd harmoni-hc
> python setup.py install
```

## Simulating high-contrast images

```MAIN.py``` creates high-contrast images, taking into account the partially corrected aberrations induced by the telescope & the instrument, as well as the effects of the shaped pupil apodizers and the focal plane masks.
These images do not, however, take into account photon noise, nor any detector-related noise.

The principal parameters of the simulation are:
- the path to the main directory, i.e., the directory in which ```MAIN.py``` is located
- the seeing [JQ1, JQ2, MED, JQ3]
- the observation band [H+K, H, K, H_high, K1_high, K2_high & debugging bands]
- the apodizer [SP1 (5 lambda/D) / SP2 (7 lambda/D)]
- the star declination
- the star mean hour angle over the observation
- the observation total duration [currently limited to 2h]

The other parameters correspond to the specifications of HARMONI & ELT. If changed, the simulations will not reflect the physics of the instrument.

The images are normalized such that the total incoming energy is spread uniformly over the ELT aperture, and over the bandwidth.

