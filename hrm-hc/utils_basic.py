import numpy as np
import os
import time
from scipy.ndimage import rotate, shift, gaussian_filter
from scipy.ndimage import shift
from scipy.interpolate import interp1d
from scipy.interpolate import interp2d
from numpy import pi
from math import gamma
import matplotlib.pyplot as plt
import sys
sys.path.insert(0, '/Users/carlotal/Downloads/proper_v3.1.1_python_3.x_15july19/')
from astropy.io import fits
from astropy.utils.exceptions import AstropyWarning
import warnings
warnings.simplefilter('ignore', category=AstropyWarning)
import imageio

WORKPATH = '/Users/carlotal/harmoni-hc/hrm-hc/COMMON_FILES/'

def ZELDA(P,PHI,lbd_vect,k_lbd,delta,D_win,D_tel,zelda_diam_mas,zelda_height,ref_index):
    #P:                 pupil
    #PHI:               phase screen (m)
    #lbd:               wavelength vector (m)
    #k_lbd:             index within wavelength vector
    #delta:             dispersion from lbd_0 (mas)
    #D_win:             physical diameter of input array (m)
    #D_tel:             physical diameter of telescope (m)
    #zelda_diam_mas:    diameter of zelda mask (mas)
    #zelda height:      height of zelda mask (m)
    #ref_index:         refraction index at lbd
    
    #  ____________ _      _____          
    # |___  /  ____| |    |  __ \   /\    
    #    / /| |__  | |    | |  | | /  \   
    #   / / |  __| | |    | |  | |/ /\ \  
    #  / /__| |____| |____| |__| / ____ \ 
    # /_____|______|______|_____/_/    \_\
                                        
    N = len(P)
    x,X,Y,R,T = VECT(N,D_win)
    lbd = lbd_vect[k_lbd]
    lbd_0 = np.mean(lbd_vect)
    N_lbd = len(lbd_vect)
    dispersion = np.exp(2*1j*np.pi*MAS2LD(delta,D_tel,lbd)*Y/D_tel) #phase due to dispersion
    D_ratio = D_win/D_tel
    lbd_ratio = lbd/lbd_0
    FOV = 1.1*MAS2LD(zelda_diam_mas,D_tel,lbd)
    # computation of electric fields in image plane
    E_zelda_pre = ft(P*np.exp(2*1j*np.pi*PHI/lbd)*dispersion,1,D_ratio,D_ratio,FOV,FOV,lbd_ratio,N,N)
    E_zelda_full = ft(P*np.exp(2*1j*np.pi*PHI/lbd)*dispersion,1,D_ratio,D_ratio,N/2,N/2,lbd_ratio,N,N)
    E_zelda_noab = ft(P*dispersion,1,D_ratio,D_ratio,FOV,FOV,lbd_ratio,N,N) #no aberrations here; used to infer the diffraction due to the mask
    # zelda mask model
    u,U,V,R,T = VECT(N,FOV)
    zelda_shape = (R<MAS2LD(zelda_diam_mas,D_tel,lbd)/2)
    zelda_mask = np.exp(2*1j*np.pi*zelda_height*(ref_index-1)/lbd)*zelda_shape
    E_zelda_post = E_zelda_pre*zelda_mask
    E_zelda_diff = E_zelda_pre*zelda_shape
    # computation of electric field in second pupil plane
    P_zelda = ft(E_zelda_post,1,FOV,FOV,D_ratio,D_ratio,lbd_ratio,N,N)
    P_zelda_diff = ft(E_zelda_diff,1,FOV,FOV,D_ratio,D_ratio,lbd_ratio,N,N)
    P_zelda_full = ft(E_zelda_full,1,N/2,N/2,D_ratio,D_ratio,lbd_ratio,N,N)
    P_camera = P_zelda_full + P_zelda - P_zelda_diff 
    # intensity in pupil plane
    I = np.abs(P_camera)**2/N_lbd
    # b factor (diffraction of zelda mask)
    b = -np.real(ft(E_zelda_noab*zelda_shape,1,FOV,FOV,D_ratio,D_ratio,lbd_ratio,N,N))/N_lbd #for later calibration    
    return (I,b)

def MyInterp2(IN,x1,x2):
    interp_f = interp2d(x1,x1,IN,kind='linear',fill_value=0)
    OUT = interp_f(x2,x2)
    return OUT

def BOUNDARIES1D(A):
    A = (A.round()).astype(int)
    # "Enclose" mask with sentients to catch shifts later on
    mask = np.r_[False,np.equal(A, 1),False]

    # Get the shifting indices
    idx = np.flatnonzero(mask[1:] != mask[:-1])

    # Get the start and end indices with slicing along the shifting ones
    return np.array(list(zip(idx[::2], idx[1::2]-1)))

def MAKE_CIF(A,Dot_size_micron,output_filename):
    
    # The second number following 'DS' should be equal to 100 times the size of
    # a pixel (in microns). Example: if the pixel size is 5 microns, then 'DS 1 500 1'
    
    A= 1.0*(A**2 >= 0.5)
    
    f = open(output_filename,"w+")
    
    #fprintf(fid, '%s\n','(RESOLUTION 1);');
    #fprintf(fid, '%s\n',['DS 1 ' num2str(round(100*Dot_size_micron)) ' 1;']);
    #fprintf(fid, '%s\n','L 1;');
    
    f.write("%s\n" % '(RESOLUTION 1);')
    f.write("%s\n" % ('DS 1 ' + str(format(np.int(np.round(100*Dot_size_micron)))) + ' 1;'))
    f.write('%s\n' % 'L 1;')

    k0=np.int(np.round(len(A)/2))
    l0=np.int(np.round(len(A)/2))
    
    for k in range(len(A)):
        B=BOUNDARIES1D(A[k,:])
        for l in range(len(B)):
            #C=B[l]
            x0=B[l,0]
            x1=B[l,1]#C[np.round(len(C)/2),2]
            f.write('%s\n' % ('P ' + str(k-k0) + ',' + str(x0-1-l0) + ' ' + str(k-k0) + ',' + str(x1-l0) + ' ' + str(k+1-k0) + ',' + str(x1-l0) + ' ' + str(k+1-k0) + ',' + str(x0-1-l0) + ';'))

    f.write('%s\n' % 'DF;')
    f.write('%s\n' % 'E')

    f.close()

def ANAMORPHOSIS(P,D,gamma):
    N = len(P)
    x = VECT1(N,D)
    x2 = VECT1(N,D*gamma[0])
    y2 = VECT1(N,D*gamma[1])
    f_R = interp2d(x,x,P.real,kind='cubic',fill_value=0)
    f_I = interp2d(x,x,P.imag,kind='cubic',fill_value=0)
    P2 = f_R(x2,y2) + 1j*f_I(x2,y2)
    return P2

def make_mp4(ims, name="", fps=20):
    print("Making mp4...")
    with imageio.get_writer("{}.mp4".format(name), mode='I', fps=fps) as writer:
        for im in ims:
            #writer.append_data(bgr2rgb(im))
            writer.append_data(im)
    print("Done")

def HALO(dsp_file_name,FOV,Min_wavelength_for_Nyquist,Im,lbd=1.6e-6,Tel_diam=38.542,angle=0.):
    '''
    dsp_file_name: filename of DSP ; a fits is expected
    FOV: field of view [arcsec]
    Min_wavelength_for_Nyquist
    lbd: wavelength [m], optional ; default is 1.6e-6
    Im: image array (intensity)
    Tel_diam: telescope diameter [m], optional ; default is ELT diameter (38.542)
    angle: rotation angle of DSP [degrees], optional
    '''
    dsp = fits.getdata(dsp_file_name)
    size_dsp = len(dsp)
    size_min = np.round(np.ceil(MAS2LD(FOV,Tel_diam,Min_wavelength_for_Nyquist)/2))*2*4

    if size_min > size_dsp :
        print('size of original dsp is {0}, but should be at least {1} to fit FoV at minimum wavelength'.format(size_dsp,size_min))
        print('Interpolating DSP to get correct size...')
        x_DSP,X_DSP,Y_DSP,R_DSP,T_DSP      = VECT(size_dsp,2)
        x2_DSP,X2_DSP,Y2_DSP,R2_DSP,T2_DSP = VECT(size_min,2)
        f_dsp = interp2d(x_DSP,x_DSP,dsp,kind='linear',fill_value=0)
        dsp = f_dsp(x2_DSP,x2_DSP)
        dsp[np.isnan(dsp)==True] = 0
        size_dsp = len(dsp)

    DiamFTO = 2 #;%400/382;%BigD/D;
    dim = size_dsp/2
    Diameter = 1
    fmax     = dim/Diameter*0.5
    f1D,Xf,Yf,Rf,Tf = VECT(size_dsp,2*fmax)
    df = np.abs(f1D[2]-f1D[1])
    SUMDSP = np.sum(dsp)*df**2
    FT_dsp = np.abs(ft(dsp,1,dim,dim,DiamFTO,DiamFTO,1,len(dsp),len(dsp)))
    MAXFTDSP = np.max(FT_dsp)
    RENORM_FT_DSP = FT_dsp/MAXFTDSP*SUMDSP
    
    FT2DSP_norm=(2*pi/lbd)**2
    FT_PST=RENORM_FT_DSP*FT2DSP_norm
    FT_PST=rotate(FT_PST,angle,reshape=False)
    PH_STR=2.*(FT_PST[np.int(size_dsp/2),np.int(size_dsp/2)]-FT_PST)
    FTO_atm=np.exp(-PH_STR/2.)
    SR=np.exp(-np.max(FT_PST))
    print('Strehl: {0}'.format(SR))
    
    OWA = MAS2LD(FOV,Tel_diam,lbd)/2
    FTO_tel_0=ft(Im,1,2*OWA,2*OWA,DiamFTO,DiamFTO,1,len(dsp),len(dsp))
    FTO_tot_0=FTO_tel_0*FTO_atm
    Im_post=np.abs(ft(FTO_tot_0,1,DiamFTO,DiamFTO,2*OWA,2*OWA,1,len(Im),len(Im)))
    
    return Im_post

def PROJECT_FIBER(Im,FoV):
    N=len(Im)
    Im_filt = np.zeros((N,N),dtype=complex)
    u,U,V,R,T=VECT(N,FoV)

    #U1,V1,U2,V2 = np.meshgrid(u,u,u,u)
    
    #R = np.sqrt((U1-U2)**2+(V1-V2)**2)
    
    for k in range(N):
        for l in range(N):
            R_loc = np.sqrt((U-u[k])**2+(V-u[l])**2)
            REG = (R_loc<1)
            Im_filt[l,k]=np.sum(Im[REG]*np.exp(-R_loc[REG]**2))
    return Im_filt

def PROP_HARMONI_E2E(endgame,lbd_wfs, lbd, ELT_pupil, gridsize, beam_width_ratio, WFE_cube, APOD_amplitude,offset):
    
    lens_diam = 2.134
    fl_exit_pupil = 37.868 #
    
    d_M6_focus_tel = 3.5
    d_exit_pupil_M6 = fl_exit_pupil - d_M6_focus_tel
    d_focus_tel_win1 = 1.85
    d_win1_win2 = 0.05
    d_win2_RM1 = 2.1
    
    #fl_exit_pupil = 0
    fl_RM1 = 2.0
    fl_RM2 = -1.0
    fl_RM3 = 2.0    
    fl_D1 = 580e-3
    fl_D2 = 600e-3
    fl_dummyLens = 250e-3
    #Distances
    
    d_RM1_RM2 = 2.0 #
    d_RM2_RM3 = 2.0 #
    d_RM3_Fold1 = 1.74 #
    d_fold1_diSCAO = 1.61 #
    d_diSCAO_FoldHC1 = 50e-3
    d_FoldHC1_FocusInHCM = 0.60
    d_FocusInHCM_FoldHC2 = 0.54
    d_FoldHC2_LHC1 = 40e-3
    d_LHC1_FoldHC3 = 90e-3
    
    ## calculs conjug pupil """"
    Pup_RM1 = fl_exit_pupil  + d_focus_tel_win1 + d_win1_win2 + d_win2_RM1
    Pup_after_RM1 = (1/fl_RM1+1/-Pup_RM1)**-1
    Pup_after_RM2 = (1/fl_RM2+1/(Pup_after_RM1-d_RM1_RM2))**-1
    Pup_after_RM3 = (1/fl_RM3+1/(Pup_after_RM2-d_RM2_RM3))**-1
    d_Pup_lHC1 = Pup_after_RM3 - d_RM3_Fold1 - d_fold1_diSCAO - d_diSCAO_FoldHC1 - d_FoldHC1_FocusInHCM - d_FocusInHCM_FoldHC2 - d_FoldHC2_LHC1
    Pup_after_LHC1 = round((1/fl_D1+1/d_Pup_lHC1)**-1, 4)
    
    d_FoldHC3_diZELDA = 310e-3
    d_diZELDA_APOD = Pup_after_LHC1-d_LHC1_FoldHC3 - d_FoldHC3_diZELDA
    
    # calcul conjug pupil ds voie ZELDA
    fl_L1Z = 1310e-3
    fl_L2Z = 120e-3
    d_diZELDA_L1Z = 208e-3
    d_L1Z_F1Z = 130e-3
    d_F1Z_F2Z = 810e-3
    d_F2Z_Zmask = 370e-3
    
    ## calculs conjug pupil in ZELDA channel """"
    Pup_afterL1Z = (1/fl_L1Z+1/-(d_diZELDA_L1Z -d_diZELDA_APOD))**-1
    Pup_afterL2Z = (1/fl_L2Z+1/(Pup_afterL1Z - fl_L1Z - fl_L2Z))**-1
    
    d_Zmask_L2Z = fl_L2Z
    d_L2Z_Zcam = round(Pup_afterL2Z, 4)
    
    #calculs pour confondre la pupille sur voie directe et HCM
    # direct channel
    #d_RM3_FocusR = 4
    #d_Pup_FocusR = Pup_after_RM3 - d_RM3_FocusR
    
    #HCM channel
    d_APOD_LHC2 = 600e-3#590.7e-3
    #d_LHC1_LHC2 = d_LHC1_FoldHC3 + d_FoldHC3_diZELDA + d_diZELDA_APOD + d_APOD_LHC2
    
    #Pup_after_LHC2 = (1/fl_D2+1/-(d_LHC1_LHC2-Pup_after_LHC1))**-1
    d_LHC2_FoldHC4 = 50e-3
    d_FoldHC4_cryowin = 400e-3
    d_cryowin_FocusR = 0.15
    
    d_dummyLens = fl_dummyLens
    
    #d_PupHCM_FocusR = Pup_after_LHC2 + d_LHC2_FoldHC4 + d_FoldHC4_cryowin + d_cryowin_FocusR
    
    if endgame == 'science':
    
        DIST_vect = [d_exit_pupil_M6,
                     d_M6_focus_tel,
                     d_focus_tel_win1,
                     d_win1_win2,
                     d_win2_RM1,
                     d_RM1_RM2,
                     d_RM2_RM3,
                     d_RM3_Fold1,
                     d_fold1_diSCAO,
                     d_diSCAO_FoldHC1,
                     d_FoldHC1_FocusInHCM,
                     d_FocusInHCM_FoldHC2,
                     d_FoldHC2_LHC1,
                     d_LHC1_FoldHC3,
                     d_FoldHC3_diZELDA,
                     d_diZELDA_APOD,
                     d_APOD_LHC2,
                     d_LHC2_FoldHC4,
                     d_FoldHC4_cryowin,
                     d_cryowin_FocusR]
    
        FOC_vect = [0,
                    0,
                    0,
                    0,
                    fl_RM1,
                    fl_RM2,
                    fl_RM3,
                    0,
                    0,
                    0,
                    0,
                    0,
                    fl_D1,
                    0,
                    0,
                    0,
                    fl_D2,
                    0,
                    0,
                    0]
        
    elif endgame == 'scao':
    
        DIST_vect = [d_exit_pupil_M6,
                     d_M6_focus_tel,
                     d_focus_tel_win1,
                     d_win1_win2,
                     d_win2_RM1,
                     d_RM1_RM2,
                     d_RM2_RM3,
                     d_RM3_Fold1,
                     d_fold1_diSCAO,
                     d_diSCAO_FoldHC1,
                     d_FoldHC1_FocusInHCM]
    
        FOC_vect = [0,
                    0,
                    0,
                    0,
                    fl_RM1,
                    fl_RM2,
                    fl_RM3,
                    0,
                    0,
                    0,
                    0]
        
    elif endgame == 'zelda':
        
        DIST_vect = [d_exit_pupil_M6,
                     d_M6_focus_tel,
                     d_focus_tel_win1,
                     d_win1_win2,
                     d_win2_RM1,
                     d_RM1_RM2,
                     d_RM2_RM3,
                     d_RM3_Fold1,
                     d_fold1_diSCAO,
                     d_diSCAO_FoldHC1,
                     d_FoldHC1_FocusInHCM,
                     d_FocusInHCM_FoldHC2,
                     d_FoldHC2_LHC1,
                     d_LHC1_FoldHC3,
                     d_FoldHC3_diZELDA,
                     d_diZELDA_L1Z,
                     d_L1Z_F1Z,
                     d_F1Z_F2Z,
                     d_F2Z_Zmask,
                     d_Zmask_L2Z,
                     d_L2Z_Zcam]
    
        FOC_vect = [0,
                    0,
                    0,
                    0,
                    fl_RM1,
                    fl_RM2,
                    fl_RM3,
                    0,
                    0,
                    0,
                    0,
                    0,
                    fl_D1,
                    0,
                    0,
                    fl_L1Z,
                    0,
                    0,
                    0,
                    fl_L2Z,
                    0]
    
    wfo = proper.prop_begin(lens_diam, lbd, gridsize, beam_width_ratio)
    proper.prop_multiply(wfo, ELT_pupil)
    RHO = MAS2LD(DISPER(lbd_wfs*1e6,lbd*1e6,45)*1000,38.542,lbd)
    x,X,Y,R,T = VECT(gridsize,1/beam_width_ratio)
    TILT = np.exp(2*1j*pi*X*RHO)
    proper.prop_multiply(wfo, TILT)
    
    proper.prop_multiply( wfo, np.exp(2*1j*pi*offset/lbd))
    
    proper.prop_define_entrance(wfo)
    proper.prop_lens(wfo,fl_exit_pupil)
    
    for k in range(len(WFE_cube[:,0,0])):
        proper.prop_propagate(wfo, DIST_vect[k])
        if np.abs(FOC_vect[k]) > 0:
            proper.prop_lens(wfo,FOC_vect[k])
        if np.sum(np.abs(WFE_cube[k,:,:])) > 0:
            proper.prop_add_phase( wfo, WFE_cube[k,:,:])
        if k==15 and endgame == 'science':
            proper.prop_multiply(wfo,APOD_amplitude)
#        if endgame == 'science':
#            AZ = proper.prop_get_amplitude(wfo)
#            plt.figure()
#            plt.imshow(AZ)
            
#        if endgame == 'science':
#            #smp = proper.prop_get_beamradius(wfo)
#            #print('Beam radius: {:4.2f}mm'.format(np.round(2*smp*1e3)))
#            smp = proper.prop_get_sampling(wfo)
#            print('Beam radius: {:4.2f}mm'.format(np.round(smp*1e3*gridsize*beam_width_ratio)))
#            
    if endgame == 'science' or endgame == 'scao':
        proper.prop_propagate(wfo, d_dummyLens)
        proper.prop_lens(wfo,fl_dummyLens)
        proper.prop_propagate(wfo, d_dummyLens)
    
    if endgame == 'science' or endgame == 'zelda':
        TILT = np.exp(2*1j*pi*X*RHO)
        proper.prop_multiply(wfo, TILT)
    else:
        TILT = np.exp(-2*1j*pi*X*RHO)
        proper.prop_multiply(wfo, TILT)
    
    #PUP_fin = proper.prop_get_wavefront(wfo)
    
    PHI = proper.prop_get_phase(wfo)
    PHI = PHI/(2*np.pi)*lbd
    
    E = proper.prop_get_wavefront(wfo)
        
    return PHI, E

def PROP_HARMONI_E2E_SCAO(endgame,lbd_wfs, lbd, ELT_pupil, gridsize, beam_width_ratio, WFE_cube):
    
    lens_diam = 2.134
    fl_exit_pupil = 37.868 #
    
    d_M6_focus_tel = 3.5
    d_exit_pupil_M6 = fl_exit_pupil - d_M6_focus_tel
    d_focus_tel_win1 = 1.85
    d_win1_win2 = 0.05
    d_win2_RM1 = 2.1
    
    #fl_exit_pupil = 0
    fl_RM1 = 2.0
    fl_RM2 = -1.0
    fl_RM3 = 2.0    
    fl_D1 = 580e-3
    fl_D2 = 600e-3
    fl_dummyLens = 250e-3
    #Distances
    
    
    d_RM1_RM2 = 2.0 #
    d_RM2_RM3 = 2.0 #
    d_RM3_Fold1 = 1.74 #
    d_fold1_diSCAO = 1.61 #
    d_diSCAO_FoldHC1 = 50e-3
    d_FoldHC1_FocusInHCM = 0.60
    d_FocusInHCM_FoldHC2 = 0.54
    d_FoldHC2_LHC1 = 40e-3
    d_LHC1_FoldHC3 = 90e-3
    
    ## calculs conjug pupil """"
    Pup_RM1 = fl_exit_pupil  + d_focus_tel_win1 + d_win1_win2 + d_win2_RM1
    Pup_after_RM1 = (1/fl_RM1+1/-Pup_RM1)**-1
    Pup_after_RM2 = (1/fl_RM2+1/(Pup_after_RM1-d_RM1_RM2))**-1
    Pup_after_RM3 = (1/fl_RM3+1/(Pup_after_RM2-d_RM2_RM3))**-1
    d_Pup_lHC1 = Pup_after_RM3 - d_RM3_Fold1 - d_fold1_diSCAO - d_diSCAO_FoldHC1 - d_FoldHC1_FocusInHCM - d_FocusInHCM_FoldHC2 - d_FoldHC2_LHC1
    Pup_after_LHC1 = round((1/fl_D1+1/d_Pup_lHC1)**-1, 4)
    
    d_FoldHC3_diZELDA = 310e-3
    d_diZELDA_APOD = Pup_after_LHC1-d_LHC1_FoldHC3 - d_FoldHC3_diZELDA
    
    # calcul conjug pupil ds voie ZELDA
    fl_L1Z = 1310e-3
    fl_L2Z = 120e-3
    d_diZELDA_L1Z = 208e-3
    d_L1Z_F1Z = 130e-3
    d_F1Z_F2Z = 810e-3
    d_F2Z_Zmask = 370e-3
    
    ## calculs conjug pupil in ZELDA channel """"
    Pup_afterL1Z = (1/fl_L1Z+1/-(d_diZELDA_L1Z -d_diZELDA_APOD))**-1
    Pup_afterL2Z = (1/fl_L2Z+1/(Pup_afterL1Z - fl_L1Z - fl_L2Z))**-1
    
    d_Zmask_L2Z = fl_L2Z
    d_L2Z_Zcam = round(Pup_afterL2Z, 4)
    
    #calculs pour confondre la pupille sur voie directe et HCM
    # direct channel
    #d_RM3_FocusR = 4
    #d_Pup_FocusR = Pup_after_RM3 - d_RM3_FocusR
    
    #HCM channel
    d_APOD_LHC2 = 600e-3#590.7e-3
    #d_LHC1_LHC2 = d_LHC1_FoldHC3 + d_FoldHC3_diZELDA + d_diZELDA_APOD + d_APOD_LHC2
    
    #Pup_after_LHC2 = (1/fl_D2+1/-(d_LHC1_LHC2-Pup_after_LHC1))**-1
    d_LHC2_FoldHC4 = 50e-3
    d_FoldHC4_cryowin = 400e-3
    d_cryowin_FocusR = 0.15
    
    d_dummyLens = fl_dummyLens
    
    #d_PupHCM_FocusR = Pup_after_LHC2 + d_LHC2_FoldHC4 + d_FoldHC4_cryowin + d_cryowin_FocusR
    
    if endgame == 'SCAO':
    
        DIST_vect = [d_exit_pupil_M6,
                     d_M6_focus_tel,
                     d_focus_tel_win1,
                     d_win1_win2,
                     d_win2_RM1,
                     d_RM1_RM2,
                     d_RM2_RM3,
                     d_RM3_Fold1,
                     d_fold1_diSCAO,
                     d_diSCAO_FoldHC1,
                     d_FoldHC1_FocusInHCM]
    
        FOC_vect = [0,
                    0,
                    0,
                    0,
                    fl_RM1,
                    fl_RM2,
                    fl_RM3,
                    0,
                    0,
                    0,
                    0]
        
    elif endgame == 'zelda':
        
        DIST_vect = [d_exit_pupil_M6,
                     d_M6_focus_tel,
                     d_focus_tel_win1,
                     d_win1_win2,
                     d_win2_RM1,
                     d_RM1_RM2,
                     d_RM2_RM3,
                     d_RM3_Fold1,
                     d_fold1_diSCAO,
                     d_diSCAO_FoldHC1,
                     d_FoldHC1_FocusInHCM,
                     d_FocusInHCM_FoldHC2,
                     d_FoldHC2_LHC1,
                     d_LHC1_FoldHC3,
                     d_FoldHC3_diZELDA,
                     d_diZELDA_L1Z,
                     d_L1Z_F1Z,
                     d_F1Z_F2Z,
                     d_F2Z_Zmask,
                     d_Zmask_L2Z,
                     d_L2Z_Zcam]
    
        FOC_vect = [0,
                    0,
                    0,
                    0,
                    fl_RM1,
                    fl_RM2,
                    fl_RM3,
                    0,
                    0,
                    0,
                    0,
                    0,
                    fl_D1,
                    0,
                    0,
                    fl_L1Z,
                    0,
                    0,
                    0,
                    fl_L2Z,
                    0]
    
    wfo = proper.prop_begin(lens_diam, lbd, gridsize, beam_width_ratio)
    proper.prop_multiply(wfo, ELT_pupil)
    RHO = MAS2LD(DISPER(lbd_wfs*1e6,lbd*1e6,45)*1000,38.542,lbd)
    x,X,Y,R,T = VECT(gridsize,1/beam_width_ratio)
    TILT = np.exp(2*1j*pi*X*RHO)
    proper.prop_multiply(wfo, TILT)
    proper.prop_define_entrance(wfo)
    proper.prop_lens(wfo,fl_exit_pupil)
    
    for k in range(len(WFE_cube[:,0,0])):
        proper.prop_propagate(wfo, DIST_vect[k])
        if np.abs(FOC_vect[k]) > 0:
            proper.prop_lens(wfo,FOC_vect[k])
        if np.sum(np.abs(WFE_cube[k,:,:])) > 0:
            proper.prop_add_phase( wfo, WFE_cube[k,:,:])
#        if endgame == 'science':
#            AZ = proper.prop_get_amplitude(wfo)
#            plt.figure()
#            plt.imshow(AZ)
            
    if endgame == 'SCAO':
        proper.prop_propagate(wfo, d_dummyLens)
        proper.prop_lens(wfo,fl_dummyLens)
        proper.prop_propagate(wfo, d_dummyLens)

    TILT = np.exp(2*1j*pi*X*RHO)
    proper.prop_multiply(wfo, TILT)
    
    #PUP_fin = proper.prop_get_wavefront(wfo)
    
    PHI = proper.prop_get_phase(wfo)
    PHI = PHI/(2*np.pi)*lbd
    
    E = proper.prop_get_wavefront(wfo)
        
    return PHI, E

def WRITE_DAT(filename,A):
    f = open(filename + '.dat', 'w')
    N = len(A)
    for k in range(N):
        for l in range(N):
            f.write("{0}".format(1.*A[k,l]))
            if l==(N-1):
                f.write("\n")
            else:
                f.write("\t")

def READ_DAT(filename):
    A = np.fromfile(filename + '.dat', dtype=float, sep='\n')
    return A

def minterp2d(x,y,A,x2,y2):
    A_f = interp2d(x,y,A,kind='linear',fill_value=0)
    B = A_f(x2,y2)
    return B

def ZERNIKE_PROJECT(WF,P,D,N_order):
    N = len(WF)
    Z_coeff = np.zeros(N_order)
    x,X,Y,R,T = VECT(N,2*D)
    for k in range(0,N_order):
        ZER = ZERNIKE(k,R,T)
        Z = ZER*WF
        Z_coeff[k]= np.sum(Z[P>0])/np.sum(P)
    return Z_coeff

def ZERNIKE(i,r,theta):
    zernike_index = fits.getdata(WORKPATH + 'zernike_index.fits')
    n = zernike_index[i,0]
    m = zernike_index[i,1]
    if m==0:
        Z = np.sqrt(n+1)*zrf(n,0,r)
    else:
        if i//2 == i/2:
            Z = np.sqrt(2*(n+1))*zrf(n,m,r)*np.cos(m*theta)
        else:
            Z = np.sqrt(2*(n+1))*zrf(n,m,r)*np.sin(m*theta)
    return Z

def zrf(n, m, r):
    R=0
    N_max = int((n-m)//2+1)
    for s in range(0,N_max):
        num = (-1)**s * gamma(n-s+1)
        denom = gamma(s+1) * gamma((n+m)/2-s+1) * gamma((n-m)/2-s+1)
        R = R + num / denom * r**(n-2*s)
    return R


def AB2V_CONVFACT(WORKPATH,lambda_0, BW):
    """
    Converts an AB_mag to a VEGA magnitude, for a given lambda_0 [nm] and a
    BW bandwidth [nm];
    It relies on the F_nu data for VEGA from the STScI (alpha_lyr_stis_003.fits)
    The product CV is such that CV=mAB-mVEGA, or said otherwise: mVEGA=mAB-CV
    """
    DATA_V=fits.getdata(WORKPATH + 'VEGA_Fnu.fits')
    print(np.shape(DATA_V))
    lbd=np.linspace(lambda_0-BW/2,lambda_0+BW/2,1000)
    dlbd=lbd[1]-lbd[0]
    F_nu_f = interp1d(DATA_V[:,0],DATA_V[:,1],fill_value=0)
    F_nu = F_nu_f(lbd)    
    S1=np.sum(F_nu*dlbd*3.34e-19*(lbd*10)**2);
    S2=np.sum(dlbd*np.ones(1000))
    CV = -2.5*np.log10(S1/S2)-48.6
    return CV

def PhotonCount(T,S,lambda_0,BW,mag,mag_type,throughput):
    """
    T:          exposure time [s]
    S:          telescope surface [m2]
    lambda_0:   central wavelength [m]
    BW:         bandwidth [m]
    mag:        apparent magnitude of the star
    mag_type:   magnitude system: either 'AB' or 'VEGA'
    throughput: telescope+instrument throughput. It does not include the coronagraph throughput.
    """
    if mag_type == 'AB':
        AB_mag=mag
    elif mag_type == 'VEGA':
        AB_mag=mag+AB2V_CONVFACT(WORKPATH,lambda_0*1e9, BW*1e9)
    else:
        AB_mag=mag
        print('Error! Choose between AB mag or VEGA. Switching to AB mag')

    PC = T*S*throughput*1.51*1e26/(1e-9*lambda_0*10)*10**(-(AB_mag+48.6)/2.5)*BW*1e-9*10*10000
    return PC

def MakeBinary(A):

    N = len(A)
    B = A

    for j in range(N):
        for i in range(N):

            old_pixel = B[i,j]
            new_pixel = np.round(B[i,j])
            B[i,j] = new_pixel
            quant_error = old_pixel-new_pixel
            
            if ((i+1)<N-1):
                B[i+1,j]=B[i+1,j]+7/16*quant_error

            if (i>0 and j<N-1):
                B[i-1,j+1]=B[i-1,j+1]+3/16*quant_error

            if ((j+1)<N-1):
                B[i,j+1]=B[i,j+1]+5/16*quant_error

            if ((i+1)<N-1 and (j+1)<N-1):
                B[i+1,j+1]=B[i+1,j+1]+1/16*quant_error
    return B

def AZAV(I,OWA,PhotAp):
    L = len(I)
    u,U,V,R,T = VECT(L,2*OWA)
    f = u[np.int(L/2+1):L]
    Lf = len(f)
    I_m = np.zeros(Lf)
    I_s = np.zeros(Lf)
    for k in range(Lf):
        REG = (R<(f[k]+PhotAp/2))*(R>(f[k]-PhotAp/2))
        I_m[k] = np.mean(I[REG==True])
        I_s[k] = np.std(I[REG==True])
    return (I_m,I_s,f)

def AZAV_STAT(I,OWA,PhotAp,fraction):
    L = len(I)
    u,U,V,R,T = VECT(L,2*OWA)
    f = u[np.int(L/2+1):L]
    Lf = len(f)
    I_m = np.zeros(Lf)
    I_sup = np.zeros(Lf)
    for k in range(Lf):
        REG = (R<(f[k]+PhotAp/2))*(R>(f[k]-PhotAp/2))
        I_m[k] = np.mean(I[REG==True])
        I_sup[k] = np.quantile(I[REG==True],fraction)
    return (I_m,I_sup,f)


def MAKE_ELT(WORKPATH,N,D,n_miss,ref_err):
    #Problem of coherence between ESO pupil and this one
    #D = D *1.021
    C = fits.getdata(WORKPATH + 'Coord_ELT.fits') 
    #
    N=N+N%2
    W=1.45*np.cos(np.pi/6.0)
    [x,X,Y,R,T]=VECT(N,D)
    #
    hexa=np.zeros([N,N])
    MISS_LIST=np.round(np.random.random(n_miss)*797)+1
    #
    for k in range(0,len(C)):
        Xt=X+C[k,0]
        Yt=Y+C[k,1]
        hexa=hexa+(k+1)*(Yt<0.5*W)*(Yt>=-0.5*W)*((Yt+np.tan(np.pi/3)*Xt)<W)*((Yt+np.tan(np.pi/3)*Xt)>=-W)*((Yt-np.tan(np.pi/3)*Xt)<W)*((Yt-np.tan(np.pi/3)*Xt)>=-W)
    #
    color=hexa
    hexa=np.double(hexa>0)
    MS = np.ones((N,N))
    #
    if n_miss > 0:
        for k in range(0,n_miss):
            MS *= (1-(color==MISS_LIST[k]))
    #
    hexa_nospider = hexa
    hexa_nospider[R<15]=1
    t=0.5
    hexa=MS*hexa*(np.abs(X)>t/2)*((Y<(np.tan(np.pi/6)*X-t/2/np.cos(np.pi/6)))+(Y>(np.tan(np.pi/6)*X+t/2/np.cos(np.pi/6))))*((Y<(np.tan(-np.pi/6)*X-t/2/np.cos(-np.pi/6)))+(Y>(np.tan(-np.pi/6)*X+t/2/np.cos(-np.pi/6))))
    
    referr = np.zeros([N,N])
    for k in range(0,798):
        referr = referr + (color==(k+1))*(1-ref_err*np.random.random())
    
    hexa = referr * hexa
    
    #
    return (hexa,color,MS,referr)

def MAKE_ELT_CALUNIT(WORKPATH,N,D):
    #Problem of coherence between ESO pupil and this one
    #D = D *1.021
    C = fits.getdata(WORKPATH + 'Coord_ELT.fits') 
    #
    N=N+N%2
    W=1.45*np.cos(np.pi/6.0)
    [x,X,Y,R,T]=VECT(N,D)
    #
    hexa=np.zeros([N,N])
    #
    for k in range(0,len(C)):
        Xt=X+C[k,0]
        Yt=Y+C[k,1]
        hexa=hexa+(k+1)*(Yt<0.5*W)*(Yt>=-0.5*W)*((Yt+np.tan(np.pi/3)*Xt)<W)*((Yt+np.tan(np.pi/3)*Xt)>=-W)*((Yt-np.tan(np.pi/3)*Xt)<W)*((Yt-np.tan(np.pi/3)*Xt)>=-W)
    #
    hexa=np.double(hexa>0)
    hexa[R<15]=1
    
    return hexa

def EELTNLS(N,a1,a2,a3):
    D=1
    d=0.3
    t=1.4/100
    Th=30*pi/180
    x,X,Y,R,T = VECT(N,D)
    a1=a1/100
    a2=a2/100
    a3=a3/100
    ELT=(R<=D/2*a1)*(R>=d/2*a2)*(((Y <= (X-t*a3/(2*np.sin(Th)))*np.tan(Th)) + (Y >= (X+t*a3/np.sin(Th)/2)*np.tan(Th)))*(X>t*a3/2)*(Y>0))
    ELT=ELT+np.fliplr(ELT)
    LS=ELT+np.flipud(ELT)
    LS=np.transpose(LS)
    return LS


def APPLY_CROSSTALK(I):
    L,M,N = np.shape(I)
    FWHM_L_pix = 2
    FWHM_X_pix = 1
    sL = FWHM_L_pix/(2*np.sqrt(2*np.log(2)))
    sX = FWHM_X_pix/(2*np.sqrt(2*np.log(2)))
    I_out = gaussian_filter(I,sigma=(sL,0,sX))
    I_mean = np.mean(I,axis=0)
    for k in range(L):
        I_out[k,:,:] = I_out[k,:,:]*(1-0.5/100)+0.5/100*I_mean
    return I_out
    

def BEAMSHIFT(N,SCREEN,Pupil_shift,D_opt_vect,Z_opt_vect,lambda_wfs,lambda_vect,alpha_zen,rot_ang_start,rot_vect,com_vect,gamma):
    #This function creates a wavefront error map and shifts it.
    N_optics = len(D_opt_vect)
    if np.shape(lambda_vect)==():
        WF = np.zeros([N,N])
        WF_NCPA = np.zeros([N,N])
        shift_vect = np.zeros(N_optics)
        alpha_disper = gamma*DISPER(1e6*lambda_wfs,1e6*lambda_vect,alpha_zen)
        alpha_disper = np.tan(alpha_disper/3600*pi/180/2)*2
        for l in range(N_optics):
            shift_vect[l] = alpha_disper*Z_opt_vect[l]/D_opt_vect[l]
        M=1358
        MNm = np.int(M/2-N/2)
        MNp = np.int(M/2+N/2)
    #1x = VECT1(len(SCREEN[:,:,1]),len(SCREEN[:,:,1])/1024)
        for l in range(N_optics):
            init_screen=SCREEN[:,:,l]
            if (rot_vect[l]==1):
                init_screen = rotate(init_screen, 90-alpha_zen-rot_ang_start, order=0, reshape=False)
            
            #interp_f = interp2d(x,x,init_screen,kind='linear',fill_value=0)
            #translated_screen = interp_f(x,x+shift[l]+Pupil_shift[l])
            pix_shift_0 = (shift_vect[l]+Pupil_shift[l])*1024
            translated_screen = shift(1.0*init_screen, [pix_shift_0,0], output=None, order=1, mode='constant', cval=0.0, prefilter=True)
            translated_screen = translated_screen[MNm:MNp,MNm:MNp]
            if (com_vect[l]==1):
                WF=WF+translated_screen
            else:
                WF_NCPA=WF_NCPA+translated_screen
    else:
        N_lambda = len(lambda_vect)
        WF = np.zeros([N,N,N_lambda])
        WF_NCPA = np.zeros([N,N,N_lambda])
        alpha_disper = np.zeros(N_lambda)
        shift_vect = np.zeros([N_lambda,N_optics])
        for k in range(N_lambda):
            alpha_disper[k] = gamma*DISPER(1e6*lambda_wfs,1e6*lambda_vect[k],alpha_zen)
            for l in range(N_optics):
                shift_vect[k,l] = np.tan(alpha_disper[k]/3600*pi/180/2)*2*Z_opt_vect[l]/D_opt_vect[l]
        M=1358
        MNm = np.int(M/2-N/2)
        MNp = np.int(M/2+N/2)
        #x = VECT1(len(SCREEN[:,:,1]),len(SCREEN[:,:,1])/1024)
        for l in range(N_optics):
            init_screen=SCREEN[:,:,l]
            if (rot_vect[l]==1):
                init_screen = rotate(init_screen, 90-alpha_zen-rot_ang_start, order=1, reshape=False)
            for k in range(N_lambda):
                #interp_f = interp2d(x,x,init_screen,kind='cubic',fill_value=0)
                #translated_screen = interp_f(x,x+shift[k,l]+Pupil_shift[l])
                pix_shift_0 = (shift_vect[k,l]+Pupil_shift[l])*1024
                translated_screen = shift(1.0*init_screen, [pix_shift_0,0], output=None, order=1, mode='constant', cval=0.0, prefilter=True)
                translated_screen = translated_screen[MNm:MNp,MNm:MNp]
                if (com_vect[l]==1):
                    WF[:,:,k]=WF[:,:,k]+translated_screen
                else:
                    WF_NCPA[:,:,k]=WF_NCPA[:,:,k]+translated_screen
    return (WF,WF_NCPA)

def M4Magic(WORKPATH,WF,P,X_off,Y_off):
    nact = 4338
    dim  = 64

    inf_func_name = WORKPATH + 'inf_func_M4.fits'
    pos_name = WORKPATH + 'positions_inf_func_M4.fits'
    MMI_name = WORKPATH + 'MMI_dd_wo_piston_4338_spider_optical_pupil.fits'
    DDWOP = fits.getdata(inf_func_name)
    DDPOSOK = fits.getdata(pos_name)
    MMI=fits.getdata(MMI_name)
    coef = np.zeros(nact)
    for k in range(nact):
        Xk = DDPOSOK[1,k]-X_off
        Yk = DDPOSOK[0,k]-Y_off
        x0 = np.int(Xk-dim/2-1)
        x1 = np.int(Xk+dim/2-1)
        y0 = np.int(Yk-dim/2-1)
        y1 = np.int(Yk+dim/2-1)
        coef[k] = np.sum(WF[x0:x1,y0:y1]*DDWOP[k,:,:]*P[x0:x1,y0:y1]) / np.sum(P)
    commands = np.dot(np.transpose(coef),np.transpose(MMI))
    WF2 = np.zeros(np.shape(P))
    for k in range(nact):
        Xk = DDPOSOK[1,k]-X_off
        Yk = DDPOSOK[0,k]-Y_off
        x0 = np.int(Xk-dim/2-1)
        x1 = np.int(Xk+dim/2-1)
        y0 = np.int(Yk-dim/2-1)
        y1 = np.int(Yk+dim/2-1)
        WF2[x0:x1,y0:y1] = WF2[x0:x1,y0:y1]+commands[k]*DDWOP[k,:,:]
    return WF2

def MPS(N,power):
    f_max = N/2
    f_vect,Xf,Yf,Rf,Tf = VECT(N,2*f_max)
    screen = np.fft.fft2(np.exp(2*1j*pi*(np.random.rand(N,N)-0.5))*Rf**(-power/2))
    KER = [[-1,1],[1,-1]]
    if N//2-N/2==0:
        KER = np.pad(KER,[np.int(N/2-1),np.int(N/2-1)],'wrap')
    else:
        KER = np.pad(KER,[np.int(N/2),np.int(N/2)],'wrap')
        KER = KER[0:N,0:N]
    screen = np.real(screen*KER)
    return screen

def MPS_DSP(M,DSP):
    # generates a random phase screen of NxN points based on a 2D array representing the PSD
    # The extent of the PSD must be 2*f_max where f_max is the maximum frequency is N/2 
    N = len(DSP)
    f_max = N/2
    f_vect,Xf,Yf,Rf,Tf = VECT(N,2*f_max)
    rand_screen = np.exp(2*1j*pi*(np.random.rand(N,N)-0.5))
    #screen = np.fft.fftshift(np.fft.fft2(rand_screen*np.sqrt(DSP)))
    screen = ft(rand_screen*np.sqrt(DSP),1,2*f_max,2*f_max,1,1,1,M,M)*(M/N)
    #KER = [[-1,1],[1,-1]]
    #if N//2-N/2==0:
    #    KER = np.pad(KER,[np.int(N/2-1),np.int(N/2-1)],'wrap')
    #else:
    #    KER = np.pad(KER,[np.int(N/2),np.int(N/2)],'wrap')
    #    KER = KER[0:N,0:N]
    #screen = np.real(screen*KER)
    screen = np.real(screen)
    #screen_ft = np.real(screen_ft)
    return screen#,screen_ft)

def DSP(WF,gamma,P,alpha_min,alpha_max):
    #WF: wavefront
    #gamma : ratio of array physical size and pupil physical size
    #P: pupil array (potentially larger than pupil)
    # alpha_min & alpha_max : min and max spatial frequencies
    [Nx,Ny] = np.shape(WF)
    WF = WF - np.mean(WF[P>0])
    OWA = Nx / 4.
    #M = 2*np.int(2*OWA/gamma)
    M = 2*np.int(2*OWA)
    u,U,V,R,T = VECT(M,2*OWA)
    du = u[1]-u[0]
    E = ft_BASIC(P*WF,gamma,2*OWA,M,1)
    I = np.abs(E)**2
    if alpha_max > OWA:
        alpha_max = OWA
    REG = (R<alpha_max)*(R>alpha_min)
    Value = np.sqrt(np.sum(I[REG])*du**2)
    return (Value,I)

def MAKE_WF_CUBE(N,D,WFE):
    # N: number of points
    # D: physical diameter of array, assuming pupil has diameter 1
    # WFE : vector of wavefront errors inside the pupil
    L = len(WFE)
    x,X,Y,R,T = VECT(N,D)
    P = (R<0.5)
    WFCube = np.zeros((L,N,N))
    for k in range(L):
        SQUARE = MPS(N,2) #assuming f-2 power here
        V,I = DSP(SQUARE,D,P,0,10000)
        WFCube[k,:,:] = SQUARE/V*WFE[k]
    return WFCube

def DISPER(lbd_min,lbd_max,theta):
    #lbd_min & lbd_max must be given in microns
    #theta must be given in degrees
    if lbd_min > lbd_max:
        new_lbd_max = lbd_min
        lbd_min = lbd_max
        lbd_max = new_lbd_max
        sign = -1
    else:
        sign = 1
    TC = 12
    T  = TC + 273.16
    RH = 15
    P  = 743
    lbd = np.linspace(lbd_min,lbd_max,2)
    PS = -10474.0+116.43*T-0.43284*T**2+0.00053840*T**3
    P2 = RH/100.0*PS
    P1 = P-P2
    D1 = P1/T*(1.0+P1*(57.90*1.0e-8-(9.3250*1.0e-4/T)+(0.25844/T**2)))
    D2 = P2/T*(1.0+P2*(1.0+3.7e-4*P2)*(-2.37321e-3+(2.23366/T)-(710.792/T**2)+(7.75141e4/T**3)))
    S0 = 1.0/lbd_min
    S  = 1.0/lbd
    N0_1 = 1.0e-8*((2371.34+683939.7/(130-S0**2)+4547.3/(38.9-S0**2))*D1+(6487.31+58.058*S0**2-0.71150*S0**4+0.08851*S0**6)*D2)
    N_1  = 1.0e-8*((2371.34+683939.7/(130-S**2)+4547.3/(38.9-S**2))*D1+(6487.31+58.058*S**2-0.71150*S**4+0.08851*S**6)*D2)
    D = sign*np.max(np.tan(theta*pi/180)*(N0_1-N_1)*206264.8)
    return D

def DISPER_ADC(lbd_min,lbd_max,theta,theta_min,theta_max): 
    D = DISPER(lbd_min,lbd_max,theta)
    D = np.abs(D-(DISPER(lbd_min,lbd_max,theta_min)+DISPER(lbd_min,lbd_max,theta_max))/2) 
    if lbd_min > lbd_max:
        D = -D
    return D

def ELEVATION(D,H):
    #Elevation function returns the elevation E of a star with a declination D when observed at an hour angle H
    #D [degrees]
    #H [hours]
    #E [degrees]
    L = -24.5892*pi/180
    E = 180/pi*np.arcsin(np.sin(L)*np.sin(D*pi/180)+np.cos(L)*np.cos(D*pi/180)*np.cos(H*15*pi/180))
    return E

def AZIMUTH(D,H):
    #Elevation function returns the elevation E of a star with a declination D when observed at an hour angle H
    #D [degrees]
    #H [hours]
    #E [degrees]
    L = -24.5892*pi/180
    A = 180/pi*np.arccos((np.cos(L)*np.sin(D*pi/180)-np.sin(L)*np.cos(D*pi/180)*np.cos(H*15*pi/180))/np.cos(np.pi/180*ELEVATION(D,H)))*np.sign(H)
    return A

def PARANG(D,H):
    L  = -24.5892
    PA = 180/pi*np.arctan(np.sin(H*15*pi/180)/(np.cos(D*pi/180)*np.tan(L*pi/180)-np.sin(D*pi/180)*np.cos(H*15*pi/180)))
    return PA

def PSF(E):
    I = np.log10(np.abs(E)**2/np.max(np.ravel(np.abs(E)**2)))
    return I

def LD2MAS(LD, D, lbd):
    Out = LD*lbd/D*180*3600*1000/pi
    return Out

def cart2pol(x, y):
    rho = np.sqrt(x**2 + y**2)
    phi = np.arctan2(y, x)
    return (rho, phi)

def pol2cart(rho, phi):
    x = rho * np.cos(phi)
    y = rho * np.sin(phi)
    return (x, y)

def VECT1(N, D):
    x = np.linspace(-D/2.0, D/2.0, N, endpoint=False) + D/(2.0*N)
    return x

def VECT(N, D):
    x = np.linspace(-D/2.0, D/2.0, N, endpoint=False) + D/(2.0*N)
    X , Y = np.meshgrid(x, x, sparse = False)
    R , T = cart2pol(X,Y)
    return (x, X, Y, R, T)

def MAS2LD(MAS, D, lbd):
    Out = MAS/(lbd/D*180*3600/pi*1000)
    return Out

def ft_BASIC(Ein, W1, W2, n2, direction):
    nx, ny = np.shape(Ein)
    dx = W1/nx
    du = W2/n2
    x = (np.linspace(-nx/2,nx/2,nx,endpoint=False)+0.5)*dx
    u = (np.linspace(-n2/2,n2/2,n2,endpoint=False)+0.5)*du
    x_u = np.outer(x,u)
    if np.abs(direction) != 1:
        print('Error: direction must equal 1 (direct FT) or -1 (inverse FT)')
        print('Setting direction to 1')
    Eout = np.dot(np.transpose(np.exp(-direction*2.*pi*1j*x_u)),Ein) 
    Eout = np.dot(Eout,np.exp(-direction*2.*pi*1j*x_u))
    Eout = Eout*dx*dx
    return Eout

def ft(Ein, z, a, b, u, v, lbd, nu, nv):
    nx, ny = np.shape(Ein)
    dx = a/nx
    du = u/nu
    x = (np.linspace(-nx/2,nx/2,nx,endpoint=False)+0.5)*dx
    u = (np.linspace(-nu/2,nu/2,nu,endpoint=False)+0.5)*du
    x_u = np.outer(x,u)
    Eout = np.dot(np.transpose(np.exp(-2.*pi*1j*x_u/lbd)),Ein) 
    Eout = np.dot(Eout,np.exp(-2.*pi*1j*x_u/lbd))
    Eout = Eout*dx*dx*np.exp(2*1j*pi*z/lbd)/(1j*lbd*z)
    return Eout

def ft_cyl(Ein, z, a, b, u, v, lbd, nu, nv):
    nx, ny = np.shape(Ein)
    dx = a/nx
    du = u/nu
    x = (np.linspace(-nx/2,nx/2,nx,endpoint=False)+0.5)*dx
    u = (np.linspace(-nu/2,nu/2,nu,endpoint=False)+0.5)*du
    x_u = np.outer(x,u)
    Eout = np.dot(np.transpose(np.exp(-2.*pi*1j*x_u/lbd)),Ein) 
    Eout = Eout*dx*np.exp(2*1j*pi*z/lbd)/np.sqrt(1j*lbd*z)
    return Eout

def ftinv(Ein, z, a, b, u, v, lbd, nu, nv):
    nx, ny = np.shape(Ein)
    dx = a/nx
    du = u/nu
    x = (np.linspace(-nx/2,nx/2,nx,endpoint=False)+0.5)*dx
    u = (np.linspace(-nu/2,nu/2,nu,endpoint=False)+0.5)*du
    x_u = np.outer(x,u)
    Eout = np.dot(np.transpose(np.exp(2.*pi*1j*x_u/lbd)),Ein) 
    Eout = np.dot(Eout,np.exp(2.*pi*1j*x_u/lbd))
    Eout = Eout*dx*dx*np.exp(-2*1j*pi*z/lbd)/(1j*lbd*z)
    return Eout

def ft_3D(Ein, z, a, b, u, v, lbd, nu, nv):
    #Loads datacube and perform ft for each spectral slice (first dimension)
    #Last 2 dimensions assumed to be X,Y
    nz, nx, ny = np.shape(Ein)
    dx = a/nx
    du = u/nu
    x = (np.linspace(-nx/2,nx/2,nx,endpoint=False)+0.5)*dx
    u = (np.linspace(-nu/2,nu/2,nu,endpoint=False)+0.5)*du
    x_u = np.outer(x,u)
    u_x = np.outer(u,x)
    t0 = time.time()
    XU_3D = np.repeat(x_u[np.newaxis,:,:],nz,axis=0)
    UX_3D = np.repeat(u_x[np.newaxis,:,:],nz,axis=0)
    LBD_XU_2D = np.repeat(lbd[:,np.newaxis],nx,axis=1)
    LBD_XU_3D = np.repeat(LBD_XU_2D[:,:,np.newaxis],nu,axis=2)
    LBD_UX_2D = np.repeat(lbd[:,np.newaxis],nu,axis=1)
    LBD_UX_3D = np.repeat(LBD_UX_2D[:,:,np.newaxis],nx,axis=2)
    LBD_UU_2D = np.repeat(lbd[:,np.newaxis],nu,axis=1)
    LBD_UU_3D = np.repeat(LBD_UU_2D[:,:,np.newaxis],nu,axis=2)
    t1 = time.time()-t0
    print(t1)
    
    print(Ein.shape)
    print(LBD_UX_3D.shape)
    print(UX_3D.shape)
    
    t0 = time.time()
    Eout = np.matmul(np.exp(-2.*pi*1j*UX_3D/LBD_UX_3D),Ein)
    #Eout = np.einsum('ijk,ikl->ijl',np.exp(-2.*pi*1j*UX_3D/LBD_UX_3D),Ein,optimize=True)
    #Eout = torch.einsum('ijk,ikl->ijl',[np.exp(-2.*pi*1j*UX_3D/LBD_UX_3D),Ein])
    t1 = time.time()-t0
    print(t1)
    
    t0 = time.time()
    Eout = np.matmul(Eout,np.exp(-2.*pi*1j*XU_3D/LBD_XU_3D))
    t1 = time.time()-t0
    print(t1)
    
    t0 = time.time()
    Eout = Eout*dx*dx*np.exp(2*1j*pi*z/LBD_UU_3D)/(1j*LBD_UU_3D*z)
    t1 = time.time()-t0
    print(t1)
    
    return Eout

def ftAMPL(Ein, a, nu, u, lbd):
    nx, ny = np.shape(Ein)
    dx = a/nx
    du = u/nu
    x = (np.linspace(-nx/2,nx/2,nx,endpoint=False)+0.5)*dx
    u = (np.linspace(-nu/2,nu/2,nu,endpoint=False)+0.5)*du
    x_u = np.outer(x,u)
    Eout = np.dot(np.transpose(np.exp(-2.*pi*1j*x_u/lbd)),Ein) 
    Eout = np.dot(Eout,np.exp(-2.*pi*1j*x_u/lbd))
    Eout = Eout/lbd*dx*dx
    return Eout

def ft1D(Ein, z, a, u, lbd, nu):
    nx = len(Ein)
    dx = a/nx
    du = u/nu
    x = (np.linspace(-nx/2,nx/2,nx,endpoint=False)+0.5)*dx
    u = (np.linspace(-nu/2,nu/2,nu,endpoint=False)+0.5)*du
    x_u = np.outer(x,u)
    Eout = np.exp(1j*np.pi*z/lbd)/(1j*np.sqrt(lbd*z))*np.dot(np.exp(-2*np.pi*1j*x_u/lbd/z),Ein)*dx
    return Eout

def Fresnel(Ein, z, a, u, lbd, nu, trigger):
    if trigger==False:
        nx = len(Ein)
        dx = a/nx
        du = u/nu
        x = (np.linspace(-nx/2,nx/2,nx,endpoint=False)+0.5)*dx
        u = (np.linspace(-nu/2,nu/2,nu,endpoint=False)+0.5)*du
        X,Y = np.meshgrid(x,x,sparse=False)
        U,V = np.meshgrid(u,u,sparse=False)
        Eout = ft(Ein*np.exp(1j*pi/(lbd*z)*(X**2+Y**2)),z,a,a,u,u,lbd,nu,nu)*np.exp(1j*pi/(lbd*z)*(U**2+V**2))
    else:    
        N = len(Ein)
        four = ft_BASIC(Ein,a,N/a,N,1)
        dx_int = 1/a
        x_int = (np.linspace(-N/2,N/2,N,endpoint=False)+0.5)*dx_int
        X_int,Y_int = np.meshgrid(x_int,x_int,sparse=False)
        angular = 1j*lbd*z*np.exp(-1j*pi*z*lbd*(X_int**2+Y_int**2))
        Eout = ft_BASIC(angular*four,N/a,u,nu,-1)
        Eout = Eout*np.exp(2*1j*pi*z/lbd)/(1j*lbd*z)
    
    return Eout

def SQ(A):
    A = np.concatenate((np.flip(A,1),A),axis=1)
    OUT = np.concatenate((np.flip(A,0),A),axis=0)
    return OUT

def VLTNLS(N,a1,a2,a3):

    D = 1.
    d = 0.14
    t = 0.09/18.
    Th = 50.5*pi/180.
    x , X , Y , R , T = VECT(N,D)
    a1 = a1/100
    a2 = a2/100
    a3 = a3/100
    VLT=1*(R<=D/2.*a1)*(R>=d/2.*a2)*(((Y <= (1*(X-d/2.+t/np.sin(Th)/2.)-1*(t*a3/np.sin(Th)/2.))*np.tan(Th)) + 1*(Y >= (1*(X-d/2.+t/np.sin(Th)/2.)+1*(t*a3/np.sin(Th)/2.))*np.tan(Th)))*(X>0.)*(Y>0.))
    VLT=VLT+np.fliplr(VLT)
    LS=VLT+np.flipud(VLT)
    return LS

def WN2WL(wavenumber):
    # wavenumber in cm-1
    # wavelength in m
    wavelength = 1/(wavenumber*100)
    return wavelength

def WL2WN(wavelength):
    # wavenumber in cm-1
    # wavelength in m
    wavenumber = 1/(wavelength*100)
    return wavenumber
